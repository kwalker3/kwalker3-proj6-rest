<html>
    <head>
        <title>CIS 322 REST-api demo: Control Times</title>
    </head>

    <body>
        <h1>Control Times</h1>
        <?php

        if(isset($_POST['button1'])) {
            $json = file_get_contents('http://laptop-service/listAll/json');
            $obj = json_decode($json);
	          $controls = $obj->controls;
            foreach ($controls as $item) {
                echo "<li>$item</li>";
            }
        }
        if(isset($_POST['button2'])) {
            $json = file_get_contents('http://laptop-service/listOpenOnly/json');
            $obj = json_decode($json);
	          $controls = $obj->controls;
            foreach ($controls as $item) {
                echo "<li>$item</li>";
            }
        }
        if(isset($_POST['button3'])) {
            $json = file_get_contents('http://laptop-service/listCloseOnly/json');
            $obj = json_decode($json);
	          $controls = $obj->controls;
            foreach ($controls as $item) {
                echo "<li>$item</li>";
            }
        }
        if(isset($_POST['button4'])) {
            $json = file_get_contents('http://laptop-service/listAll/csv');
            $obj = json_decode($json);
            echo $obj;
        }
        if(isset($_POST['button5'])) {
            $json = file_get_contents('http://laptop-service/listOpenOnly/csv');
            $obj = json_decode($json);
            echo $obj;
        }
        if(isset($_POST['button6'])) {
            $json = file_get_contents('http://laptop-service/listCloseOnly/csv');
            $obj = json_decode($json);
            echo $obj;
        }

    ?>

    <form method="post">
        <input type="submit" name="button1"
                value="ListAllJSON"/>

        <input type="submit" name="button2"
                value="ListOpenJSON"/>

        <input type="submit" name="button3"
                value="ListCloseJSON"/>

        <input type="submit" name="button4"
                value="ListAllCSV"/>

        <input type="submit" name="button5"
                value="ListOpenCSV"/>

        <input type="submit" name="button6"
                value="ListCloseCSV"/>
    </form>

    </body>
</html>
