"""
=== Project 6 Version ===
Edited by Kayla Walker
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

from pymongo import MongoClient

client = MongoClient("db", 27017)
db = client.brevetdb

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    brevet_dist = request.args.get('brevet_dist', 200, type=int)

    begin_date = request.args.get('begin_date', "2017-01-01", type=str)
    begin_time = request.args.get('begin_time', "00:00", type=str)
    # now convert begin date and begin time into arrow, then into string
    begin_arrow = arrow.get(begin_date + ' ' + begin_time, 'YYYY-MM-DD HH:mm')
    app.logger.debug("begin_arrow={}".format(begin_arrow))

    open_time = acp_times.open_time(km, brevet_dist, begin_arrow.isoformat())
    close_time = acp_times.close_time(km, brevet_dist, begin_arrow.isoformat())
    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)


@app.route('/_display')
def _display():
    _items = db.brevetdb.find()
    items = [item for item in _items]

    return render_template('display_page.html', items=items)


@app.route('/_submit')
def _submit():

    db.brevetdb.drop()

    km_list = request.args.getlist('km[]')
    open_list = request.args.getlist('open[]')
    close_list = request.args.getlist('close[]')

    print(km_list)
    print(open_list)
    print(close_list)

    for i in range(len(km_list)):
        item_doc = {
            'km': int(km_list[i]),
            'open': open_list[i],
            'close': close_list[i]
        }
        db.brevetdb.insert_one(item_doc)

    return redirect(url_for('index'))


class ListAllJSON(Resource):
    def get(self):
        _items = db.brevetdb.find({}, {'km': 1, 'open': 1, 'close': 1, '_id': 0}).sort([('km', 1)])
        items = [item for item in _items]
        for i in range(len(items)):
            items[i] = str(items[i])
        return {"controls": items}


class ListOpenJSON(Resource):
    def get(self):
        db.brevetdb.find().sort([('km', 1)])
        #parse query request
        top = request.args.get("top")
        if top is not None:
            top = int(top)
            _items = db.brevetdb.find({}, {'close': 0, '_id': 0}).limit(top).sort([('km', 1)])
        else:
            _items = db.brevetdb.find({}, {'close': 0, '_id': 0}).sort([('km', 1)])
        items = [item for item in _items]
        for i in range(len(items)):
            items[i] = str(items[i])
        return {"controls": items}


class ListCloseJSON(Resource):
    def get(self):
        #parse query request
        top = request.args.get("top")
        if top is not None:
            top = int(top)
            _items = db.brevetdb.find({}, {'open': 0, '_id': 0}).limit(top).sort([('km', 1)])
        else:
            _items = db.brevetdb.find({}, {'open': 0, '_id': 0}).sort([('km', 1)])
        items = [item for item in _items]
        for i in range(len(items)):
            items[i] = str(items[i])
        return {"controls": items}


class ListAllCSV(Resource):
    def get(self):
        #create a list of all km distances
        kms = db.brevetdb.find({}, {'km': 1, '_id': 0}).sort([('km', 1)])
        kmarray = [item for item in kms]
        kmlist = []
        for i in kmarray:
            kmlist.append(i['km'])

        #create a list of all open times
        opens = db.brevetdb.find({}, {'open': 1, '_id': 0}).sort([('km', 1)])
        opensarray = [item for item in opens]
        openlist = []
        for i in opensarray:
            openlist.append(i['open'])

        #create a list of all close times
        closes = db.brevetdb.find({}, {'close': 1, '_id': 0}).sort([('km', 1)])
        closesarray = [item for item in closes]
        closelist = []
        for i in closesarray:
            closelist.append(i['close'])

        app.logger.debug("kms={}".format(kmlist))
        app.logger.debug("opens={}".format(openlist))
        app.logger.debug("closes={}".format(closelist))

        csv_string = "km,open,close" + '<br>'

        for i in range(len(kmlist)):
            csv_string += str(kmlist[i]) + ","
            csv_string += str(openlist[i]) + ","
            csv_string += str(closelist[i]) + '<br>'

        app.logger.debug("csv_string={}".format(csv_string))
        return csv_string


class ListOpenCSV(Resource):
    def get(self):
        #parse query request
        top = request.args.get("top")
        if top is not None:
            top = int(top)
            kms = db.brevetdb.find({}, {'km': 1, '_id': 0}).limit(top).sort([('km', 1)])
            opens = db.brevetdb.find({}, {'open': 1, '_id': 0}).limit(top).sort([('km', 1)])
        else:
            kms = db.brevetdb.find({}, {'km': 1, '_id': 0}).sort([('km', 1)])
            opens = db.brevetdb.find({}, {'open': 1, '_id': 0}).sort([('km', 1)])
        kmarray = [item for item in kms]
        kmlist = []
        for i in kmarray:
            kmlist.append(i['km'])

        #create a list of all open times
        opensarray = [item for item in opens]
        openlist = []
        for i in opensarray:
            openlist.append(i['open'])

        #app.logger.debug("kms={}".format(kmlist))
        #app.logger.debug("opens={}".format(openlist))

        csv_string = "km,open" + '<br>'

        for i in range(len(kmlist)):
            csv_string += str(kmlist[i]) + ","
            csv_string += str(openlist[i]) + '<br>'

        app.logger.debug("csv_string={}".format(csv_string))
        return csv_string


class ListCloseCSV(Resource):
    def get(self):
        #parse query request
        top = request.args.get("top")
        if top is not None:
            top = int(top)
            kms = db.brevetdb.find({}, {'km': 1, '_id': 0}).limit(top).sort([('km', 1)])
            closes = db.brevetdb.find({}, {'close': 1, '_id': 0}).limit(top).sort([('km', 1)])

        else:
            kms = db.brevetdb.find({}, {'km': 1, '_id': 0}).sort([('km', 1)])
            closes = db.brevetdb.find({}, {'close': 1, '_id': 0}).sort([('km', 1)])
        #create a list of all km distances

        kmarray = [item for item in kms]
        kmlist = []
        for i in kmarray:
            kmlist.append(i['km'])

        #create list of all close times
        closesarray = [item for item in closes]
        closelist = []
        for i in closesarray:
            closelist.append(i['close'])

        #app.logger.debug("kms={}".format(kmlist))
        #app.logger.debug("closes={}".format(closelist))

        csv_string = "km,close" + '<br>'

        for i in range(len(kmlist)):
            csv_string += str(kmlist[i]) + ","
            csv_string += str(closelist[i]) + '<br>'

        #app.logger.debug("csv_string={}".format(csv_string))
        return csv_string


# Create routes
# Another way, without decorators
api.add_resource(ListAllJSON, '/listAll', '/listAll/json')
api.add_resource(ListOpenJSON, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(ListCloseJSON, '/listCloseOnly', '/listCloseOnly/json')
api.add_resource(ListAllCSV, '/listAll/csv')
api.add_resource(ListOpenCSV, '/listOpenOnly/csv')
api.add_resource(ListCloseCSV, '/listCloseOnly/csv')

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)
