# Project 6: Brevet time calculator service

CIS 322 Project, Fall 2019

Updated by Kayla Walker, kwalker3@uoregon.edu

Simple listing service from Project 5 stored in MongoDB database.

## What is in this repository

* Brevet calculator application

* Consumer program

* Dockerfile

* docker-compose.yml

## Functionality implemented in Project 6

* RESTful service to expose what is stored in MongoDB. Three basic APIs:
    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

* Two different representations: one in csv and one in json. For the above, JSON is default representation for the above three basic APIs. 
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* Query parameter to get top "k" open and close times. "Top" was interpreted to mean "first", so the items were sorted by control distance. For examples, see below.
    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

* Consumer program using PHP.
    * The consumer program can be found at localhost:5000. Clicking one of the six buttons will display brevet control data from one of the 3 basic APIs above in either CSV or JSON format. 

## Brevet calculator application

The calculator can be found at localhost:5001

Randonneurs USA (RUSA) defines a brevet as "A challenging 200-, 300-, 400-, 600-, 1000- or 1200- kilometer ride, each with a specific time limit. The randonneur carries a brevet card, which is signed and stamped at each checkpoint along the way to prove they have covered the distance successfully."

This calculator uses algorithms provided by RUSA to determine the opening and closing times of checkpoints according to their distance from the start of the ride, and replaces this calculator: https://rusa.org/octime_acp.html

The user can also save control distances and times to a database and can display the contents of the database on a separate page

### How does this calculator work?

Input: Brevet length, start time, control point distances from the start.

Output: Asynchronously outputs open and close times of controls as user inputs control distance from start.

Submit: Saves output to database, overwrites any previous data. If no entries, will show popup error message.

Display: Shows contents of database on new page. If database is blank, will show blank page.

### Permissible Brevet Lengths

Per RUSA regulations, there are 200km, 300km, 400km, 600km, and 1000km rides. The last checkpoint should be no less than the regulated distance, but no more than 20% longer (though the calculator will still return open and close times even if the checkpoint is too far). Furthermore, each brevet distance has a set closing time even if the final checkpoint is farther than the official distance.

For example, a 200km brevet's last checkpoint can be anywhere between 200km and 240km away from the start, but not 199km or 241km. A 200km brevet will always end 13hr30min after the start of the ride, regardless of whether the final checkpoint is at 200km or 220km or anywhere in between.

| Official brevet distance | Final checkpoint distance  | Final close time  |
| ------------------------ | -------------------------- | ----------------- |
| 200km                    | 200km - 240km              | 13hr 30min        |
| 300km                    | 300km - 360km              | 20hr 00min        |
| 400km                    | 400km - 480km              | 27hr 00min        |
| 600km                    | 600km - 720km              | 40hr 00min        |
| 1000km                   | 1000km - 1200km            | 75hr 00min        |


### The Algorithm

| Checkpoint distance from start (km) | Minimum speed (km/hr)  | Maximum speed (km/hr)  |
| ----------------------------------- | ---------------------- | ---------------------- |
| 0 - 199* | 15 | 34   |
| 200 - 399 | 15  | 32 |
| 400 - 599  | 15 | 30   |
| 600 - 999  | 11.428 | 28   |
| 1000 - 1200  | 13.333 | 26   |

For each control entered, the calculator uses the table above and the input start time to run the following operation:

   1. If the input distance is in miles, the calculator will convert to kilometers. Round distance to the nearest kilometer (.5km will round down).
   2. For the amount of km in each range, divide the distance by its corresponding maximum speed in the table. For example, a control at 550km is 200/34 + 200/32 + 150/30 = 17.1323. The maximum speed changes through different parts of the ride according to the table.
   3. The result is in hours, convert any decimal part into minutes, then round to the nearest minute. For instance, 17.1323hrs will convert to 17hrs 08min.
   4. Add resulting hours and minutes to the start time to determine the opening time of the control.
   5. Repeat steps using minimum speed to determine the closing time of the control, though if the control is beyond the distance of the brevet length, the final close time is capped (see table under Permissible Brevet Lengths). Furthermore, For the starting checkpoint and any checkpoint before 60km, close times are calculated with a different algorithm: A minimum speed of 20km/hr is used, and an hour is added to the calculated close time to allow for an adequate window in which to start the ride.
